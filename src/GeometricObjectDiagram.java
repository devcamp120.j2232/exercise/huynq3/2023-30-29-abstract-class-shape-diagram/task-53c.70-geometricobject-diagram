import models.Circle;
import models.ResizableCircle;

public class GeometricObjectDiagram {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(1.0);
        ResizableCircle resizablecircle = new ResizableCircle(5.0);
        System.out.println("Circle 1: " + circle);
        System.out.println("Circle 2: " + resizablecircle);
        System.out.println("DT Circle  1: " + circle.getArea());
        System.out.println("CV Circle 1: " + circle.getPerimeter()); 
        System.out.println("DT Circle 2 ban dau: " + resizablecircle.getArea());
        System.out.println("CV Circle 2 ban dau: " + resizablecircle.getPerimeter());
        resizablecircle.resize(50);
        System.out.println("DT Circle 2 resize: " + resizablecircle.getArea());
        System.out.println("CV Circle 2 resize: " + resizablecircle.getPerimeter());
    }
}
